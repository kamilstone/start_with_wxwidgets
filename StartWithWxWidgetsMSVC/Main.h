#pragma once

class App :
	public wxApp
{
public:
	App();
	virtual ~App();
	virtual bool OnInit() override;
};