# How to start with wxWidgets-Phoenix ?

This MSVC 2017 solution includes some of samples projects showing how to start up with wxWidgets. I'm wrriting down this short tutorial to face with my past problems while reading, I have to say really good [documentation](https://www.wxwidgets.org/docs/tutorials/) in case of wxWidgets.

## Table of Contents:
> a) [Meet with requirements](#req),
> b) [set up environment (two way of working)](#env),
> c) [project descriptions](#proj),
> d) go on with code.

## Requirements{#req}:
* willingness, patience and free time,
* locale machine with installed Windows/Linux Oparation System _(OSX could be too, but I won't test it)_,
* some or many free GB of free disc space, depends on method desribed below,
* _(optional)_ For me is better to understand what something was created for, ie. lib or DLL. If You don't understand what something here is about, I preffer to expand Your knowledge.

## Environment {#env}
There are to ways to start with external <<a>your language> library/module:
* Build from sources on own machine,
* Download released binaries from, in this case, wxWidgets servers.

The second way is the fastest to run _no customized_ project. It means, that You have to compile libraries from sources when You didn't find required release or want to minimize final application size etc.
For now I will show You how to do _fast travel_. It is result of many hours spent on trying resolve internal problems with building libraries on TDD, GNU-C or MinGW, on Windows and Linux. Restrictions sometimes are too specific to mach all in one case. It depends on OS actualization packages, version of OS, C++ compiler and wxWidgets libraries.

### a) Start with released binaries of libraries
`Target: fast set up (shared libs) on Windows 10 and Microsoft Visual Studio IDE`
* The main adventage of wxWidgets is cross-platform feature, so choosing OS is not a restriction.
* Choose on which architecutre You want to run Your application (x86, amd64 or both). Each one has own binaries, what should be clear [why](www.google.com).
* Choose type way of working. If You wonna debug Your application under developing or just see the result of compiled code.
* Download lastest stable, in this case DLL, release from [page](https://wxwidgets.org/downloads/), regarding to the previous selecting. _(under development version isn't worse than stable according to authors comments)_
* Download and install C++ toolchain, I mean "compiler" for your OS.
* _(optional)_ Add environment variable focusing directory of unpacked to `include` and `lib` wxWidgets binaries.
* You could try to reconstruct my _set-up_ described in next [sub-paragraph](#setup).

 #### My set-up {#setup}:
  * OS:Windows 10 Pro (build 1709)
  * IDE: 2017 Community Edition v14.1 (build 15.5.7)
  * DLLs: wxWidgets x86-x64 3.1.1 (Release and Debug)
  * Envronment system variable: named as `WXWIN` focusing on directory (of `lib` and `include` super-folder).

### b) Build Your own version of wxWidgets libraries
 * `Target: set up customized (libs) on Windows 10 and Microsoft Visual Studio IDE`

 _TBD (perhaps)_

 * `Target: set up customized libs on`

   _TBD (perhaps)_
 
##  Projects{#proj}:
* EmptyMSVCProject:

   `Target: simple frame/window of based on shared wxWidgets libs, on Win10 and MSVC 17 IDE`

   This project represent the way of creating simple C++ from skratch based on my [setup](#setup).
   * Open `New project` in MSVC 2017,
   * Choose from dialog menu `Installed -> Visual C++ -> General`,
   * Pick up `Blank project`, create `new solution`, check `create catalog for solution`,
   * Click on the name of Your project by Mouse-Right-Click and choose `Properties`,
   * In menu of new pop-up dialog window in `Configuration properties -> General` remember to check suitable `Charachter set` for Your DLLs,
   * In menu of new pop-up dialog window in `Configuration properties -> VC++ Directories` add `$(WXWIN)\include` and `$(WXWIN)\include\msvc` to `Include directories`, then to `Library directories` add path to `$(WXWIN)\lib\vc_dll`
   * Now create empty source _*.cpp_ and header _*.h_ files inside project, 
   * Click on the name of Your project by Mouse-Right-Click and choose `Properties`,
   * In menu of new pop-up dialog window in `Configuration properties -> C/C++ -> Preprocessor` add definition `WXUSINGDLL`. 
   * Apply all changes and save solution. Now You are done with configuration!
   * To compile application You have to add some lines of code. Good luck :)
   * When You are ready with code and compilation passed, your executable app want to have missing DLLs in their directory. For basic frame `wxbase_.dll` and `wxmsw_core.dll` will be mandatory. Keep in mind that names of shared libraries depend on compiled options, and may be different. 

## Contact
Author:
> Boar Artist

Mail:  
* ` boar.artist@outlook.com  `
  
Repos:
*  [Bitbucket account](https://bitbucket.org/kamilstone/)

 2017-2018 � All rights reserved for _Author_. 